//
//  CharacterTests.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 29/03/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import XCTest

class CharacterTests: XCTestCase {

    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCreateRomanization() {
        
        let testCharacter = "道 道 [dou6] [dao4] /direction/way/road/path/principle/truth/morality/reason/skill/method/Dao (of Daoism)/to say/to speak/to talk/"
        
        let separated = split(testCharacter, allowEmptySlices: false, maxSplit: 4) { $0 == " " }
        
        //  For this format, the romanizations are items 2/3.
        //  Some provider will build these later, so fix these tests then.
        let jyutString = String(filter(separated[2].generate()) { $0 != "[" && $0 != "]" })
        let pinString  = String(filter(separated[3].generate()) { $0 != "[" && $0 != "]" })
        
        let jyutping = Romanization(character: jyutString)
        let pinyin   = Romanization(character: pinString)
        
        XCTAssertEqual(jyutping.phoneme, "dou")
        XCTAssertEqual(jyutping.tone as Int!, 6)
        
        XCTAssertEqual(pinyin.phoneme, "dao")
        XCTAssertEqual(pinyin.tone as Int!, 4)
    }

    func createCharacter() {
        
        let testCharacter = "道 道 [dou6] [dao4] /direction/way/road/path/principle/truth/morality/reason/skill/method/Dao (of Daoism)/to say/to speak/to talk/"
        
        let separated = split(testCharacter, allowEmptySlices: false, maxSplit: 4) { $0 == " " }
        
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

}
