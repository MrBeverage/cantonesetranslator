//
//  DictionaryTests.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 29/03/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit
import XCTest

class DictionaryTests: XCTestCase {

    lazy var dictionary : ChineseDictionary = Yedict(
        dictionaryName: "Yedict.dat",
        supplimentalDictionaryName: "Yedict-Suppliment.dat",
        transformDictionaryName: "Yedict-Transform.dat")
    
    //  Yeah, this is dumb, but works for now.
    let expectedDictionarySize : Int = 9027
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testCreateYedict() {
        XCTAssertEqual(dictionary.size(), expectedDictionarySize)
    }

    func testGetCharacter() {
        let character = dictionary.getSimplifiedCharacters("道")![0]
        
        XCTAssertNotNil(character)
        XCTAssertEqual(character.traditional, character.simplified, "道")
    }
    
    func testUsesSupplimentalDictionary() {
        let character = "嘅"
        
        XCTAssertNotNil(dictionary.getTraditionalCharacter(character))
    }
    
    func testYedictCreationPerformance() {
        self.measureBlock() {
            //let dictionary = Yedict(dictionaryName: "Yedict.dat")
        }
    }
}
