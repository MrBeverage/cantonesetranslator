//
//  StringUtils.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 02/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

struct StringUtils {
    
    static let smallSpaceCharacter : Character = Character(UnicodeScalar(0x2006))
    static let cjkSpaceCharacter : Character = Character(UnicodeScalar(0x3000))
    
    //  Unfortunately these are not continugous values.
    //  Also, non-literal constants make xcode freak out in a way that raises a
    //  compile-blocking error.  Have to break it up within a closure.
    static let exponentTable = { () -> [Int:String] in
        var table = [
            0 : String(UnicodeScalar(0x2070)),
            1 : String(UnicodeScalar(0x00B9)) ]
        table += [
            2 : String(UnicodeScalar(0x00B2)),
            3 : String(UnicodeScalar(0x00B3)) ]
        table += [
            4 : String(UnicodeScalar(0x2074)),
            5 : String(UnicodeScalar(0x2075)) ]
        table += [
            6 : String(UnicodeScalar(0x2076)),
            7 : String(UnicodeScalar(0x2077)) ]
        table += [
            8 : String(UnicodeScalar(0x2078)),
            9 : String(UnicodeScalar(0x2079)) ]
        return table;
    }()
    
    static let chineseCharacterCodePages : [(low: UInt32, high: UInt32)] =
        [ (0x4E00, 0x9FFF), (0x3400, 0x4DFF), (0x20000, 0x2A6DF), (0x2F800, 0x2FA1F) ]
    
    static let chinesePunctuationCodePages : [(low: UInt32, high: UInt32)] =
        [ (0x3000, 0x303F) ]
    
    internal static func getExponentCharacter(exponent : Int) -> String? {
        return exponentTable[exponent]
    }
    
    internal static func isChineseCharacter(character : Character) -> Bool {
        return isWithinCodePages(character, codePages: chineseCharacterCodePages)
    }
    
    internal static func isChinesePunctuation(character : Character) -> Bool {
        return isWithinCodePages(character, codePages: chinesePunctuationCodePages)
    }
    
    internal static func isWithinCodePages(character : Character, codePages : [(low: UInt32, high: UInt32)]) -> Bool {
        
        let unicodeValue = getUnicodeValue(character)
        
        for pageRange in codePages {
            if (unicodeValue >= pageRange.low && unicodeValue <= pageRange.high) {
                return true
            }
        }
        
        return false
    }
    
    internal static func isAscii(character : Character) -> Bool {
        return getUnicodeValue(character) < 256
    }
    
    internal static func getUnicodeValue(character : Character) -> UInt32 {
        let scalars = String(character).unicodeScalars
        return scalars[scalars.startIndex].value
    }
}
