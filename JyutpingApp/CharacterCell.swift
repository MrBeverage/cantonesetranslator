//
//  CharacterCell.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 03/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CharacterCell : UICollectionViewCell {
    
    @IBOutlet private weak var toneGraph: ToneGraph!
    @IBOutlet private weak var romanizationText: UILabel!
    @IBOutlet private weak var characterText: UILabel!
    
    //  A single cell could represent many possible translations:
    var candidateCharacters : [ChineseCharacter] = []
    var selectedCharacter : ChineseCharacter? = nil
    var selectedCharacterIndex = -1
    
    func setCharacter(characters: [ChineseCharacter], index: Int) {
        
        candidateCharacters = characters
        selectedCharacterIndex = index
        selectedCharacter = characters[index]
        
        toneGraph.setTone(selectedCharacter!.jyutping[0].tone)
        romanizationText.text = buildRomanizationString(selectedCharacter!.jyutping[0])
        characterText.text = selectedCharacter!.traditional
        
        toneGraph.setNeedsDisplay()
    }
    
    func setNonChineseText(text: String) {
        
        candidateCharacters = []
        selectedCharacterIndex = -1
        selectedCharacter = nil
        
        toneGraph.tone = nil
        romanizationText.text = text
        characterText.text = ""
        
        toneGraph.setNeedsDisplay()
    }
    
    func buildRomanizationString(romanization : Romanization) -> String {
        
        var output = romanization.phoneme
        
        if (romanization.tone != nil) {
            output += StringUtils.getExponentCharacter(romanization.tone!)!
        }
        
        return output
    }
}