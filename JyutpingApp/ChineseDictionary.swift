//
//  DictionaryProvider.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 29/03/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

public protocol ChineseDictionary {
    
    func addCharacterMapping(character: ChineseCharacter)
    
    func getSimplifiedCharacters(simplifiedCharacter : String) -> [ChineseCharacter]?
    func getTraditionalCharacter(traditionalCharacter : String) -> ChineseCharacter?
    func getAllCharacters() -> [String]

    func addTransform(mandarinText: String, cantoneseText: String)
    
    func getMandarinTransform(mandarinText : String) -> String?
    func getCantoneseTransform(cantoneseText : String) -> String?
    func getTransforms() -> [String]
    
    func size() -> Int
}