//
//  Tone.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 03/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

public class Romanization {
    let phoneme : String
    let tone : Int?
    
    init(character: String) {
        //  Split the character into its phoneme and tone.
        //  For now just assume that the tone is always a single digit as the last
        //  character in the character string.
        let strlen = count(character)
        
        if strlen > 1 {
            let splitIndex = advance(character.startIndex, strlen - 1)
            
            self.tone = character[splitIndex ..< character.endIndex].toInt()
            
            if self.tone != nil {
                //  The dictionary is not very clean.  Not every pronounciation will come
                //  with a proper tone symbol.
                self.phoneme = character[character.startIndex ..< splitIndex]
            } else {
                self.phoneme = character
            }
        } else {
            //  This might not even be necessary depending on how nicely the range operations
            //  work in swift.  Test that later.
            self.phoneme = character
            self.tone = nil
        }
    }
}
