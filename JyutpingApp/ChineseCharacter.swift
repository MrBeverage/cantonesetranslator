//
//  Character.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 29/03/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

public class ChineseCharacter {
    let traditional : String
    let simplified : String
    let jyutping : [Romanization]
    let mandarin : [Romanization]
    let definitions : String
    
    convenience init(character: String, jyutping: [Romanization], mandarin: [Romanization], definitions: String) {
        self.init(traditional: character, simplified: character, jyutping: jyutping, mandarin: mandarin, definitions: definitions)
    }
    
    init(traditional: String, simplified: String, jyutping: [Romanization], mandarin: [Romanization], definitions: String) {
                
        self.traditional = traditional
        self.simplified = simplified
        self.jyutping = jyutping
        self.mandarin = mandarin
        self.definitions = definitions
    }
}
