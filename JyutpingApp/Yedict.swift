//
//  Yedict.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 29/03/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

class Yedict : SimpleChineseDictionary, ChineseDictionary {
    
    init(dictionaryName: String, supplimentalDictionaryName: String, transformDictionaryName: String) {
        super.init()
        
        self.loadDictionary(supplimentalDictionaryName)
        self.loadDictionary(dictionaryName)
        self.loadTransforms(transformDictionaryName)
    }
    
    private func loadDictionary(dictionaryName: String) {
        let fileContent = getDataFileContentLines(dictionaryName)
        
        for line in fileContent {
            var tokens = split(line, allowEmptySlices: false, maxSplit: 4) { $0 == " " }
            
            var traditional = tokens[0]
            var simplified  = tokens[1]
            
            var jyutpingPronounciations = parseRomanizations(tokens[2])
            var pinyinPronounciations   = parseRomanizations(tokens[3])
            
            var character = ChineseCharacter(traditional: traditional, simplified: simplified, jyutping: jyutpingPronounciations, mandarin: pinyinPronounciations, definitions: tokens[4])
            
            self.addCharacterMapping(character)
        }
    }
    
    private func loadTransforms(transformDictionaryName: String) {
        let fileContent = getDataFileContentLines(transformDictionaryName)
        
        for line in fileContent {
            var tokens = split(line, allowEmptySlices: false, maxSplit: 2) { $0 == " " }
            
            var mandarin = tokens[0]
            var cantonese = tokens[1]
            
            self.addTransform(mandarin, cantoneseText: cantonese)
        }
    }
    
    private func getDataFileContentLines(fileName : String) -> [String] {
        let fileContent = getDataFileContent(fileName)
        return split(fileContent, allowEmptySlices: false) { $0 == "\n" }
            .filter( { (line: String) -> Bool in line[line.startIndex] != "#" })
    }
    
    private func getDataFileContent(fileName : String) -> String {
        let path = NSBundle.mainBundle().pathForResource(fileName, ofType: nil)
        let fileContent = NSString(contentsOfFile: path!, encoding: NSUTF8StringEncoding, error: nil) as! String
        
        return fileContent
    }
    
    private func parseRomanizations(let inputString: String) -> [Romanization] {
        //  Input string is of the format [rom1 rom2 ... romn]
        let int = String(filter(inputString.generate()) { $0 != "[" && $0 != "]" })
        let strings = split(int) { $0 == "|" }
        
        var romanizations : [Romanization] = []
        
        for value in strings {
            romanizations.append(Romanization(character: value))
        }
        
        return romanizations
    }
}