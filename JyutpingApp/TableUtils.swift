//
//  TableUtils.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 02/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

func +=<K, V> (inout left: Dictionary<K, V>, right: Dictionary<K, V>) -> Dictionary<K, V> {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
    return left
}
