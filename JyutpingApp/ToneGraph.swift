//
//  ToneGraph.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 05/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit

@IBDesignable
class ToneGraph: UIView {

    @IBInspectable var tone : Int? = 0
    
    let cantoneseToneLines: [Int : (start: Int, end: Int)] =
    [   1 : (start: 1, end: 1),
        2 : (start: 3, end: 1),
        3 : (start: 2, end: 2),
        4 : (start: 3, end: 4),
        5 : (start: 3, end: 2),
        6 : (start: 3, end: 3)  ]
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        if (tone != nil && cantoneseToneLines[tone!] != nil) {
            let h = rect.height
            let w = rect.width
            
            let left = rect.width / 5
            let right = rect.width * 4 / 5
            let gap = rect.height / 5
            
            // Draw the tone staff:
            var line = UIBezierPath()
            
            for i in 1...4 {
                let iter = CGFloat(i)
                line.moveToPoint(CGPoint(x: left, y: iter * gap))
                line.addLineToPoint(CGPoint(x: right, y: iter * gap))
            }
            
            UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1).setStroke()
            line.stroke()
            
            // Draw the tone mark:
            let markerLine = UIBezierPath()
            let toneMarker : (start: Int, end: Int) = cantoneseToneLines[tone!]!
            
            let test = CGFloat(toneMarker.start)
            
            markerLine.moveToPoint(CGPoint(x: left, y: gap * CGFloat(toneMarker.start)))
            markerLine.addLineToPoint(CGPoint(x: right, y: gap * CGFloat(toneMarker.end)))
            
            UIColor.redColor().setStroke()
            markerLine.lineWidth = line.lineWidth * 2
            markerLine.stroke()
        }
    }
    
    func setTone(tone: Int?) {
        self.tone = tone
    }
}
