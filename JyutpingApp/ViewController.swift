//
//  ViewController.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 27/03/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import UIKit
import TTTAttributedLabel

class ViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    @IBOutlet weak var originalText: UITextField!
    @IBOutlet weak var characterDisplay: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var dictionary : ChineseDictionary?
    
    private var lastTranslation: [Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        characterDisplay.delegate = self
        characterDisplay.dataSource = self
        characterDisplay.registerNib(UINib(nibName: "CharacterCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "CharacterCell")
        
        view.addSubview(activityIndicator)
        activityIndicator.bringSubviewToFront(view)
        activityIndicator.startAnimating()
        originalText.enabled = false
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.dictionary = Yedict(
                dictionaryName: "Yedict.dat", supplimentalDictionaryName: "Yedict-Suppliment.dat",transformDictionaryName: "Yedict-Transform.dat");
            
            dispatch_async(dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                self.originalText.enabled = true
            }
        }
        
        characterDisplay.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func textChanged(sender: AnyObject) {
        lastTranslation = translateAndSplitInputString(originalText.text)
        characterDisplay.reloadData()
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count(lastTranslation)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CharacterCell", forIndexPath: indexPath) as! CharacterCell
        
        var object: Any = lastTranslation[advance(lastTranslation.startIndex, indexPath.item)]
        
        if let character = object as? ChineseCharacter {
            cell.setCharacter([character], index: 0)
        } else {
            cell.setNonChineseText(object as! String)
        }
        
        return cell
    }
    
    private func translateAndSplitInputString(inputString: String) -> [Any] {
        
        var textToTranslate = inputString
        var translationObjects : [Any] = [ ]
        
        //  Perform any character transformations.
        //  These may be multi-character, but there are not a lot of them, so do them
        //  all as string substitutions before translating.
        for transformableCharacters in dictionary!.getTransforms() {
            var transform: String? = dictionary!.getMandarinTransform(transformableCharacters)
            
            if (transform != nil) {
                textToTranslate = textToTranslate.stringByReplacingOccurrencesOfString(
                    transformableCharacters, withString: transform!)
            }
        }
        
        var nonChineseStringBuilder : String = ""
        
        for c in textToTranslate {
            var characters = dictionary!.getSimplifiedCharacters(String(c))
            var character : ChineseCharacter?
            
            if (characters == nil) {
                character = dictionary!.getTraditionalCharacter(String(c))
            } else {
                character = characters![0]
            }
            
            //  For now just use the first pronounciation available.  This is good for almost
            //  all characters.
            if (character != nil && count(character!.jyutping) > 0) {
                if count(nonChineseStringBuilder) > 0 {
                    //  If we've just finished a non-Chinese character sequence, add that to
                    //  the object chain and clear the temporary variable.
                    translationObjects.append(nonChineseStringBuilder)
                    nonChineseStringBuilder = ""
                }
                translationObjects.append(character!)
            } else {
                //  Could be either punctuation, or a non-Chinese string.  Chinese puncutation
                //  gets a single container as do non-Chinese strings.
                if (StringUtils.isChinesePunctuation(c)) {
                    if (count(nonChineseStringBuilder) > 0) {
                        //  If we've just finished a non-Chinese character sequence, add that to
                        //  the object chain and clear the temporary variable.
                        translationObjects.append(nonChineseStringBuilder)
                        nonChineseStringBuilder = ""
                    }
                    translationObjects.append(String(c))
                } else {
                    if (c == StringUtils.smallSpaceCharacter) {
                        translationObjects.append(nonChineseStringBuilder)
                        nonChineseStringBuilder = ""
                    }
                    nonChineseStringBuilder.append(c)
                }
            }
        }
        
        if (count(nonChineseStringBuilder) > 0) {
            translationObjects.append(nonChineseStringBuilder)
        }
        
        return translationObjects
    }
}
