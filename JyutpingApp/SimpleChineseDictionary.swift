//
//  SimpleChineseDictionary.swift
//  JyutpingApp
//
//  Created by Alex Beverage on 16/04/15.
//  Copyright (c) 2015 Alex Beverage. All rights reserved.
//

import Foundation

public class SimpleChineseDictionary : ChineseDictionary {
    
    //  Simplified:Traditional can be 1:*.  Traditional:Simplified is always 1:1:.
    var traditionalDictionary = [String: ChineseCharacter]()
    var simplifiedDictionary  = [String: [ChineseCharacter]]()
    
    //  Dictionaries for both directions for later:
    var transformFromMandarin  = [String: String]()
    var transformFromCantonese = [String: String]()
    
    public func addCharacterMapping(character: ChineseCharacter) {
        traditionalDictionary.updateValue(character, forKey: character.traditional)
        
        if (simplifiedDictionary[character.simplified] != nil) {
            simplifiedDictionary[character.simplified]?.append(character)
        } else {
            simplifiedDictionary.updateValue([character], forKey: character.simplified)
        }
    }
    
    public func getSimplifiedCharacters(simplifiedCharacter: String) -> [ChineseCharacter]? {
        return simplifiedDictionary[simplifiedCharacter]
    }
    
    public func getTraditionalCharacter(traditionalCharacter : String) -> ChineseCharacter? {
        return traditionalDictionary[traditionalCharacter]
    }
    
    public func getAllCharacters() -> [String] {
        return traditionalDictionary.keys.array
    }
    
    public func addTransform(mandarinText: String, cantoneseText: String) {
        transformFromMandarin[mandarinText] = cantoneseText
        transformFromCantonese[cantoneseText] = mandarinText
    }
    
    public func getMandarinTransform(mandarinText : String) -> String? {
        return transformFromMandarin[mandarinText]
    }
    
    public func getCantoneseTransform(cantoneseText : String) -> String? {
        return transformFromMandarin[cantoneseText]
    }
    
    public func getTransforms() -> [String] {
        return transformFromMandarin.keys.array
    }
    
    public func size() -> Int {
        return traditionalDictionary.count
    }
}